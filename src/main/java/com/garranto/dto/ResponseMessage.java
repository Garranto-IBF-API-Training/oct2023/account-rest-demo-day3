package com.garranto.dto;

public class ResponseMessage {

    private String status;
    private String description;

    public ResponseMessage(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
