package com.garranto.dto;

public class AccountBalance {

    private double availableBalance;

    private String currencyCode;

    public AccountBalance(double availableBalance, String currencyCode) {
        this.availableBalance = availableBalance;
        this.currencyCode = currencyCode;
    }

    public double getAvailableBalance() {
        return availableBalance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}
