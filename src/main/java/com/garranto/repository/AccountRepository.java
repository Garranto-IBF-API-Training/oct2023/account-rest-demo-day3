package com.garranto.repository;

import com.garranto.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account,String> {
}


//@Component @Repository @Service @Controller @RestContoller -- telling spring to create and manage corresponding beans
//@Autowiring