package com.garranto.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Account {

    @Id
    private String accNumber;

    @Column(name = "accountType", nullable = false)
    private String accType;
    private String accHolderName;

    private double balance;

    private String currency;

    public Account(String accNumber, String accType, String accHolderName, double balance, String currency) {
        this.accNumber = accNumber;
        this.accType = accType;
        this.accHolderName = accHolderName;
        this.balance = balance;
        this.currency = currency;
    }

    public Account(){}

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccHolderName() {
        return accHolderName;
    }

    public void setAccHolderName(String accHolderName) {
        this.accHolderName = accHolderName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accNumber='" + accNumber + '\'' +
                ", accType='" + accType + '\'' +
                ", accHolderName='" + accHolderName + '\'' +
                ", balance=" + balance +
                ", currency='" + currency + '\'' +
                '}';
    }
}
